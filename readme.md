
<p align="center">
	<a href="https://www.smartchart.cn"><img src="http://smartchart.cn/static/smartui/img/smartviplogo.png" width="45%"></a>
</p>
<p align="center">
	<strong>A NoBI platform that Connect Data to Insight.</strong>
</p>
<p align="center">
	<a href="https://www.smartchart.cn">https://www.smartchart.cn</a>
</p>

<p align="center">
    <img src="https://img.shields.io/badge/Release-V6.0-green.svg" alt="Downloads">
	<a target="_blank" href="https://www.python.org/downloads/release/python-390/">
		<img src="https://img.shields.io/badge/Python-3.6+-green.svg" />
	</a>
	<a href='https://gitee.com/smartchart/smartchart/members'><img src='https://gitee.com/smartchart/smartchart/badge/fork.svg?theme=dark' alt='fork'></a>
	<a href='https://gitee.com/smartchart/smartchart/stargazers'><img src='https://gitee.com/smartchart/smartchart/badge/star.svg?theme=dark' alt='star'></a>
	<a target="_blank" href='https://github.com/JohnYan2017/Echarts-Django'>
		<img src="https://img.shields.io/github/stars/JohnYan2017/Echarts-Django.svg?style=social" alt="github star"/>
	</a>
</p>
<p align="center">
	<a href="https://qm.qq.com/cgi-bin/qm/qr?k=eC34KwVvEtMvfh8Zyn1RSfYlzZvuvm7i&jump_from=webapi"><img src="https://img.shields.io/badge/QQ群-476715246-orange"/></a>
   <a target="_blank" href="https://www.smartchart.cn">
   <img src="https://img.shields.io/badge/Author-John%20Yan-ff69b4.svg" alt="Downloads">
 </a>
 <a target="_blank" href="https://www.smartchart.cn">
   <img src="https://img.shields.io/badge/Copyright%20-@smartchart.cn-%23ff3f59.svg" alt="Downloads">
 </a>
</p>

### 功能特色
- 数据可视化,大屏,移动报表,数据中台,WEB应用的微代码NoBI(No Only BI)开发平台
- 简单, 敏捷, 高效, 通用化, 高度可定制化, 让你的项目瞬间档次提升
- 完全真正打通前后端, 支持图形数据联动,筛选,钻取, 支持几乎常见的所有数据库
- 积木式拖拽开发模式, 开箱即用, 安装简单, 依赖少, 适应各种平台
- 支持中国式报表类EXCEL开发, 支持3D场景大屏
- 内存加速技术, 让你的数据快人一步, 大幅减少数据库压力
- 真所见即所得的拖拽开发模式, 且无需在画布上设计
- 支持数据填报设计,数据下载,前端埋点,问卷开发
- 数据集即服务, 采用低代码快速实现数据服务API开发
- 支持仪表盘备份恢复快照等, 满足企业级的版本控制开发上线流程要求
- 支持用户/组功能权限控制,支持行级别/字段级别数据权限控制
- 支持Django插件方式应用,可无限扩展,打造你专属的个性化应用
- 支持在Jupyter notebook中的数据开发方式应用
- 支持集成chatGPT, 文心一言等大模型AI生成
- 没有重复学习成本, 高度可定制化, 注意是高度可定制化!!