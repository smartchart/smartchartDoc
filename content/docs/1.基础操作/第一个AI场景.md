---
title: 第一个AI场景
weight: 5
type : docs
bookFlatSection : false
---
### 使用前请观看视屏
- [chatgpt辅助开发](https://www.bilibili.com/video/BV1Xc411g7Lq)
- [chatgpt业务自助探索](https://www.bilibili.com/video/BV1Bo4y1T7BU/)

### 开启GPT功能
- 仅6.6.6以上版本支持
- 在smartchart首页"头像"位置下拉菜单中, 点击"服务配置"
- 输入以下配置, 你的识别码请观看视屏(关注,点赞,转发)后联系客服获取

````json
{
  "smtgpt": {
    "api_key": "你的识别码"
  }
}
````

### 新建仪表盘
- 在开发模式下，滑动“开发管理” ->"仪表盘"-> "新增Dashboard"
- 填写相应信息后点击“保存”
- 点击进入仪表盘开发, 注意点击 "E" 字!!

### 导入GPT模板
- 在开发页面的"设定"图标下拉菜单中, 点击"常规设定"
- 点击"上传/本地快照|下载/恢复快照"
- 输入KEY: 02_GPTTABLE, 点击"本地恢复" 即可

### 模板修改
- 仪表盘开发页面点击"模板"
- 修改或者新增选项,对应的区域内容为你的表名或者数据集ID
```html
<el-select v-model="select" slot="prepend" placeholder="请选择">
  <el-option label="王者荣耀排名" value="smartdemo2"></el-option>
  <el-option label="评论数据集" value="532"></el-option>
</el-select>
```

现在你可以进行chatgpt数据问答了, 更多应用请参考第6章节"集成 AI 生成"







