---
weight: 3
---

需要在高级中可开启datav模式

参考视屏
- [1. DataV基础应用](https://www.ixigua.com/7021858988451070500)
- [2. DataV翻盘器](https://www.ixigua.com/7022086509562069512)

DataV配置方法文档:
[图表](http://datav.jiaminghi.com/guide/charts.html)
