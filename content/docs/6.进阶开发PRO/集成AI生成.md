---
weight: 6
type : docs
bookFlatSection : false
---

### 应用场景
- 集成chatgpt, 文心一言, 星火等大模型技术
- 可以支持smartchart专有AI生成

### 使用方法
- 当前smartchart已内置了生成模型供大家尝试, 视屏关注获取
- 在模板/数据集开发/容器开发/图形开发中都可以找到如下图标, 输入你的问题, 选中后点即可生成, 注意测试用途较慢, 请耐心
![输入图片说明](https://foruda.gitee.com/images/1680767755203353730/028044de_5500438.png "屏幕截图")
- 如果你需要要数据集中进行数据问答, 可以选中一段sql或表名后点击菜单"工具"-->"GPT场景",即可开启
- 如果你有chatgpt,或其它大模型的接口, 你也可以直接接入, 接入方法见下方


### 接入商业大模型
#### 使用azure chatgpt
在首页--> 头像位置下拉菜单 --> 服务配置, 输入并修改为你的以下配置即可使用
```json
{
  "smtgpt": {
    "url": "chatgpt",
    "api_type": "azure",
    "api_base": "https://xxx.openai.azure.com/",
    "api_version": "2023-03-15-preview",
    "api_key": "xxxxxxxxxx",
    "engine": "xxxx"
  }
}
```

#### 使用openai chatgpt
在首页--> 头像位置下拉菜单 --> 服务配置, 输入并修改为你的以下配置即可使用
```json
{
  "smtgpt": {
    "api_type": "open_ai",
    "api_base": "https://api.openai.com/v1",
    "api_key": "xxxxxx",
    "engine": "gpt-3.5-turbo"
  }
}
```

#### 其它大模型
比如迅飞的GPT, 首先新建一个文件, 如gptxinhuo.py
写入python脚本
```python
def gpt_dataset(prompt, **kwargs):
    #你的处理过程
    
    return {'msg': message, 'token': 0, 'status': 200}
```
在任意仪表盘开发界面--> 模板 --> 工具 --> 文件上传 菜单中将此文件上传即可
之后AI生成服务将采用你自定义的服务, 如果不熟悉python脚本, 可联系客服定制
```json
{
  "smtgpt": {
    "api_type": "gptxinhuo",
    "appid": "xxx",
    "api_key": "xxxx",
    "api_secret": "xxx",
    "api_base": "ws://spark-api.xf-yun.com/v1.1/chat"
  }
}
```
