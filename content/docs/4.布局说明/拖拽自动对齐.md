---
weight: 2
type : docs
bookFlatSection : false
---
拖拽很方便, 但是精确对齐还是有些手抖

### 网格辅助
- 你可以点击仪表盘工具栏中的info 图标切换网格显示
- 每一格为1rem

### 自动对齐
你可以在"模板" --> "转化" 中找到这个功能
- 首先我们随意拖拽了一些组件
![输入图片说明](https://images.gitee.com/uploads/images/2022/0228/092103_b34d6921_5500438.png "屏幕截图.png")
- 然后选中拖拽代码段, 点"拖拽对齐" 后 点"保存"
![拖拽对齐](https://images.gitee.com/uploads/images/2022/0228/092808_786834d5_5500438.png "屏幕截图.png")
- 就可以查看对齐后的效果了, 再进行下拖拽微调, 重复以上动作到满意
![输入图片说明](https://images.gitee.com/uploads/images/2022/0228/093116_c5154e1f_5500438.png "屏幕截图.png")
