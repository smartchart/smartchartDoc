---
weight: 2
type : docs
bookFlatSection : false
---
### 前言
- 使用最广泛的数据集一般都是SQL数据集
- 标准数据集可以想象为类EXCEL的二维表,有行和列
- 直接在数据集开发界面填写SQL即可
- 更多数据集说明查看"特殊数据源"
- 动态传参数据集查看"参数及联动"章节

### 标准图形的数据集格式
#### A类数据源
如果你的原始数据库中表的格式如下, 表名tb_name

|城市 |  户型 |  数量|
| :-----| :----: | :----: |
|长沙|   A   |   12|
|长沙 |   A  |   23|
|上海 |   B  |   19|

查询的sql: 
```sql
select 城市,户型,sum(数量) AS 数量 
from tb_name group by 城市,户型
```
正常查询的结果为
```javascript
[['城市','户型','数量'],
 ['长沙','A',35],
 ['上海','B ',19]]
```

由于生成的数据格式第二行是[字符,字符,数值],后台会智能进行转列动作, 
生成图表更容易使用格式:
```javascript
[['Categroy','A','B'],
 ['长沙',     35, 0],
 ['上海',     0, 19]]
```
> 注意: 在数据集预览可能会看到是第一种查询结果, 但到图形中其实是第二种智能转化过的了

#### B类数据源
再比如表的数据格式, 指标是展开的:

|城市 |  A  |  B|
| :-----| :----: | :----: |
|长沙 |   10 |   12|
|上海 |   11  |  19|
|长沙 | 9 |   10|

我们可以写的sql是
```sql
select 城市, sum(A) as A, sum(B) as B 
from tb_name group by 城市
```
这样得到的结果和我们标准格式也是一样的
```javascript
[['城市','A','B'],
 ['长沙', 19, 22],
 ['上海', 11, 19]]
```


### SQL多段查询
有时一个数据集可能只用一个SQL查询还不够，比如你需要一个清单数据，同时你需要一个汇总数据做为说明在图形中显示，这样你就需要使用多条SQL语句，在数据集中的写法你只需要用分号隔开，如：
``` javascript
//数据集中的查询
select ... from xxx;
select ..... from xxxxxxx

// 传递到图形中的格式为:
{"df0":[[...]]. "df1":[[......]]}
df0, df1分别对应的是第一段和第二段查询
```
[多段查询使用视屏参考](https://www.ixigua.com/6928570043022180876)