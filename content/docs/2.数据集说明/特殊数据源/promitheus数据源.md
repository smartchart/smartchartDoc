### 数据源配置方法
驱动: prometheus
连接地址: http://ip:9090


### 查询方法
输入常规查询命令即可
![输入图片说明](https://foruda.gitee.com/images/1671000557419608356/884543ec_5500438.png "屏幕截图")
返回值为prometheus标准json, 你需要在图形编辑器中使用js处理数据
```javascript
let dataset=__dataset__;
//获取返回值列表中的第一值
dataset = dataset.data.result[0].value;
print(dataset)
```

### 处理数据样列
如获得的数据
```json
{
"status":"success",
"data":{
   "resultType":"vector",
   "result":[
        {"metric":{"__name__":"starrocks_fe_routine_load_jobs","group":"fe","instance":"xxx:8030","job":"StarRocks_Cluster01","state":"CANCELLED"},
          "value":[1671188429.573,"0"]
        }, 
        {"metric":{"__name__":"starrocks_fe_routine_load_jobs","group":"fe","instance":"xxx:8030","job":"StarRocks_Cluster01","state":"NEED_SCHEDULE"},
         "value":[1671188429.573,"2"]}
        ...
       ]
    }
}
```
图形中处理成smartchart标准格式
```javascript
//处理prometheus
let df = __dataset__;
let result = df.data.result;
let dataset = [['instance','state','qty']]; //二维表头
for(let item of result){
    let pmetric = item.metric;
    let pvalue = item.value;
    dataset.push([pmetric.instance,pmetric.state,pvalue[1]]);
}
dataset = ds_pivot(dataset); //列转行
```
如果带时序的, 数据处理参考
```javascript
let df = __dataset__;
let result = df.data.result;
let dataset = [['instance','seq','qty']]; //二维表头
for(let item of result){
    let pmetric = item.metric;
    let pvalues = item.values;
    let startv = pvalues[0][1]; //初始值
    for(let i=1; i<pvalues.length; i++){
        let pvalue = pvalues[i];
        dataset.push([i,pmetric.instance,pvalue[1]-startv]); //计算增长值
    }
}
dataset = ds_pivot(dataset); //列转行
```
你可能会用上的时间戳转文本格式
```javascript
function getLocalTime(nS) {
 return new Date(nS * 1000).toLocaleString(); 
}
```

### 支持多段查询
使用分号(;)分隔查询
![输入图片说明](https://foruda.gitee.com/images/1671000954955755281/9efc6cb9_5500438.png "屏幕截图")

