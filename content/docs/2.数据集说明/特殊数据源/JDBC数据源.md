##### 应用场景
当无法在python中找到连接库时, 你还可以采用jdbc的连接方式

##### 使用方法
- 首先您需要安装jdbc的包
```
pip install JayDeBeApi
```

- 在任意的一个仪表盘的 "模板" 编辑器中, 点击上传资源的图标上传相应的jdbc JAR包即可

![输入图片说明](https://images.gitee.com/uploads/images/2022/0615/175513_ce22cb11_5500438.png "屏幕截图.png")

- 新建连接的方式, 以下使用impala为例
![输入图片说明](https://images.gitee.com/uploads/images/2022/0615/175248_5faa86d4_5500438.png "屏幕截图.png")

