---
weight: 1
type : docs
bookFlatSection : false
---
#### API接口要求
接口返回一定要是JSON格式
#### 数据源配置方法
你可选择任意数据连接

#### 使用方法
以下是简单的get及post方法样列
```js
//GET 方法:
dataset= {
"url":"https://www.smartchart.cn/smartdata/api/?i=loaddataset1&j=1"
}

//POST 方法:
dataset= {
"url":"https://www.smartchart.cn/smartdata/api",
"method":"POST",
"data":{"i":"loaddataset1", "j":"1"}
 ...
}

```

可以传入参数做出联动效果

 ```js
dataset= {
"url":"https://www.smartchart.cn/smartdata/api",
"method":"POST",
"data":{"i":"loaddataset1", "j":"/*$参数名*/"}
 ...
}
```

可以增加header等认证方式

 ```js
dataset= {
"url":"https://www.smartchart.cn/smartdata/api",
"method":"GET",
"headers":{"Cookie":"xxxxxxx"}
 ...
}
```